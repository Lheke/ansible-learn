ansible-playbook [core 2.14.3]
  config file = /home/leke/Bootcamp-Projects/ansible-learn/ansible.cfg
  configured module search path = ['/home/leke/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  ansible collection location = /home/leke/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/bin/ansible-playbook
  python version = 3.10.6 (main, Nov 14 2022, 16:10:14) [GCC 11.3.0] (/usr/bin/python3)
  jinja version = 3.0.3
  libyaml = True
Using /home/leke/Bootcamp-Projects/ansible-learn/ansible.cfg as config file
host_list declined parsing /home/leke/Bootcamp-Projects/ansible-learn/hosts as it did not pass its verify_file() method
script declined parsing /home/leke/Bootcamp-Projects/ansible-learn/hosts as it did not pass its verify_file() method
auto declined parsing /home/leke/Bootcamp-Projects/ansible-learn/hosts as it did not pass its verify_file() method
Parsed /home/leke/Bootcamp-Projects/ansible-learn/hosts inventory source with ini plugin
Skipping callback 'default', as we already have a stdout callback.
Skipping callback 'minimal', as we already have a stdout callback.
Skipping callback 'oneline', as we already have a stdout callback.

PLAYBOOK: deploy-docker.yaml ***************************************************
2 plays in deploy-docker.yaml

PLAY [Install python3, docker, docker-compose] *********************************

TASK [Gathering Facts] ********************************************************************************************************
task path: /home/leke/Bootcam<34.229.124.83> ESTABLISH SSH CONNECTION FOR USER: ec2-user
<34.229.124.83> SSH: EXEC ssh -C -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o 'IdentityFile="/home/leke/.ssh/id_rsa"' -o KbdInteractiveAuthentication=no -o PreferredAuthentications=gssapi-with-mic,gssapi-keyex,hostbased,publickey -o PasswordAuthentication=no -o 'User="ec2-user"' -o ConnectTimeout=10 -o 'ControlPath="/home/leke/.ansible/cp/c8ccd02d7b"' 34.229.124.83 '/bin/sh -c '"'"'echo ~ec2-user && sleep 0'"'"''
<34.229.124.83> (0, b'/home/ec2-user\n', b'')
<34.229.124.83> ESTABLISH SSH CONNECTION FOR USER: ec2-user
<34.229.124.83> SSH: EXEC ssh -C -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o 'IdentityFile="/home/leke/.ssh/id_rsa"' -o KbdInteractiveAuthentication=no -o PreferredAuthentications=gssapi-with-mic,gssapi-keyex,hostbased,publickey -o PasswordAuthentication=no -o 'User="ec2-user"' -o ConnectTimeout=10 -o 'ControlPath="/home/leke/.ansible/cp/c8ccd02d7b"' 34.229.124.83 '/bin/sh -c '"'"'( umask 77 && mkdir -p "` echo /home/ec2-user/.ansible/tmp `"&& mkdir "` echo /home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291 `" && echo ansible-tmp-1678214393.5239315-129585-209041966736291="` echo /home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291 `" ) && sleep 0'"'"''
<34.229.124.83> (0, b'ansible-tmp-1678214393.5239315-129585-209041966736291=/home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291\n', b'')
Using module file /usr/lib/python3/dist-packages/ansible/modules/setup.py
<34.229.124.83> PUT /home/leke/.ansible/tmp/ansible-local-129581fr_ght44/tmpvb0lq4vw TO /home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291/AnsiballZ_setup.py
<34.229.124.83> SSH: EXEC sftp -b - -C -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o 'IdentityFile="/home/leke/.ssh/id_rsa"' -o KbdInteractiveAuthentication=no -o PreferredAuthentications=gssapi-with-mic,gssapi-keyex,hostbased,publickey -o PasswordAuthentication=no -o 'User="ec2-user"' -o ConnectTimeout=10 -o 'ControlPath="/home/leke/.ansible/cp/c8ccd02d7b"' '[34.229.124.83]'
<34.229.124.83> (0, b'sftp> put /home/leke/.ansible/tmp/ansible-local-129581fr_ght44/tmpvb0lq4vw /home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291/AnsiballZ_setup.py\n', b'')
<34.229.124.83> ESTABLISH SSH CONNECTION FOR USER: ec2-user
<34.229.124.83> SSH: EXEC ssh -C -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o 'IdentityFile="/home/leke/.ssh/id_rsa"' -o KbdInteractiveAuthentication=no -o PreferredAuthentications=gssapi-with-mic,gssapi-keyex,hostbased,publickey -o PasswordAuthentication=no -o 'User="ec2-user"' -o ConnectTimeout=10 -o 'ControlPath="/home/leke/.ansible/cp/c8ccd02d7b"' 34.229.124.83 '/bin/sh -c '"'"'chmod u+x /home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291/ /home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291/AnsiballZ_setup.py && sleep 0'"'"''
<34.229.124.83> (0, b'', b'')
<34.229.124.83> ESTABLISH SSH CONNECTION FOR USER: ec2-user
<34.229.124.83> SSH: EXEC ssh -C -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o 'IdentityFile="/home/leke/.ssh/id_rsa"' -o KbdInteractiveAuthentication=no -o PreferredAuthentications=gssapi-with-mic,gssapi-keyex,hostbased,publickey -o PasswordAuthentication=no -o 'User="ec2-user"' -o ConnectTimeout=10 -o 'ControlPath="/home/leke/.ansible/cp/c8ccd02d7b"' -tt 34.229.124.83 '/bin/sh -c '"'"'sudo -H -S -n  -u root /bin/sh -c '"'"'"'"'"'"'"'"'echo BECOME-SUCCESS-qedenkioblhfthlptggkyduolpunkcww ; /usr/bin/python3 # to define which python to use /home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291/AnsiballZ_setup.py'"'"'"'"'"'"'"'"' && sleep 0'"'"''
Escalation succeeded
<34.229.124.83> (255, b'Python 3.7.16 (default, Dec 15 2022, 23:24:54) \r\n[GCC 7.3.1 20180712 (Red Hat 7.3.1-15)] on linux\r\nType "help", "copyright", "credits" or "license" for more information.\r\n\x1b[?1034h>>> ', b'Shared connection to 34.229.124.83 closed.\r\n')
<34.229.124.83> ESTABLISH SSH CONNECTION FOR USER: ec2-user
<34.229.124.83> SSH: EXEC ssh -C -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o 'IdentityFile="/home/leke/.ssh/id_rsa"' -o KbdInteractiveAuthentication=no -o PreferredAuthentications=gssapi-with-mic,gssapi-keyex,hostbased,publickey -o PasswordAuthentication=no -o 'User="ec2-user"' -o ConnectTimeout=10 -o 'ControlPath="/home/leke/.ansible/cp/c8ccd02d7b"' 34.229.124.83 '/bin/sh -c '"'"'rm -f -r /home/ec2-user/.ansible/tmp/ansible-tmp-1678214393.5239315-129585-209041966736291/ > /dev/null 2>&1 && sleep 0'"'"''
<34.229.124.83> (0, b'', b'')
fatal: [34.229.124.83]: UNREACHABLE! => {
    "changed": false,
    "msg": "Failed to connect to the host via ssh: Shared connection to 34.229.124.83 closed.",
    "unreachable": true
}

PLAY RECAP *********************************************************************
34.229.124.83              : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   

